#import cv2 as cv
from math import sin, cos
import copy
import matplotlib.pyplot as plt
from pylab import *
from mpl_toolkits.mplot3d import Axes3D

PI = 3.14159265

# Matrice rotation autour de x
theta_x = 0.0 / 180.0 * PI #radian
R_x = [[1,0,0],[0,cos(theta_x),-sin(theta_x)],[0,sin(theta_x),cos(theta_x)]]

# Matrice rotation autour de y
theta_y = 45.0 / 180.0 * PI #radian
R_y = [[cos(theta_y),0,sin(theta_y)],[0,1,0],[-sin(theta_y),0,cos(theta_y)]]

# Matrice rotation autour de z
theta_z = 0.0 / 180.0 * PI #radian
R_z = [[cos(theta_z),-sin(theta_z),0],[sin(theta_z),cos(theta_z),0],[0,0,1]]

# Matrice de translation
t_x = 0.0
t_y = 0.0
t_z = 10.0
T = [t_x,t_y,t_z]

# Matrice Rotation ie fait le produit matriciel R = R_x*R_y*Rz

def RotationMatrix():
    R = []
    R2 = []
    for k in range(0,3):
        R_tmp = []
        for i in range(3):
            R_tmp.append(float("%.2f"%(R_x[k][0]*R_y[0][i] + R_x[k][1]*R_y[1][i] + R_x[k][2]*R_y[2][i])))
        R.append(R_tmp)

    for k in range(0,3):
        R_tmp = []
        for i in range(3):
            R_tmp.append(float("%.2f"%(R[k][0]*R_z[0][i] + R[k][1]*R_z[1][i] + R[k][2]*R_z[2][i])))
        R2.append(R_tmp)

    return R2

R = RotationMatrix()
#print(R)

# Matrice parametres extrinseques ie définit M = [R T]

def ExtrinsequeMatrix():
    M_ex = []
    for k in range(0,3):
        M_ex.append(R[k])
        M_ex[k].append(T[k])
    M_ex.append([0,0,0,1])
    return M_ex

M_ex = ExtrinsequeMatrix()
#print(M_ex)

# Example
#x_e = 5
#y_e = 3
#z_e = 2
#p = [x_e,y_e,z_e,1]
p=[1,0,1,1]

# Coordonnees homogenes camera (x_c,y_c,z_c,1)
def CameraMatrix():
    C = []
    for k in range(4):
        C.append(float("%.2f"%(M_ex[k][0]*p[0] + M_ex[k][1]*p[1] + M_ex[k][2]*p[2] + M_ex[k][3]*p[3])))
    return C

C = CameraMatrix()
#print(C)

# projection perspective sur la camera (x_c',y_c',1) normalisee
focal = 25
P_c = [float("%.2f"%(focal*C[0]/p[2])),float("%.2f"%(focal*C[1]/p[2])),C[2]/p[2]]

#print(P_c)

# Matrice parametres intrinseques
k_x = 640.0/6.4
k_y = 480.0/4.8
u0 = 320
v0 = 240
P_in = [[1/k_x,0,u0],[0,1/k_y,v0],[0,0,1]]
#print(P_in)

# Projection dans l'image
Q = []
for k in range(3):
    Q.append(float("%.2f"%(P_in[k][0]*P_c[0] + P_in[k][1]*P_c[1] + P_in[k][2]*P_c[2])))
for k in range(3):
    Q[k] /= Q[2]
    Q[k] = float("%.2f"%(Q[k]))

#print(Q)

def newProj1Point(x_e,y_e,z_e):
    p = [x_e,y_e,z_e,1]

    C = []
    for k in range(4):
        C.append(float("%.2f"%(M_ex[k][0]*p[0] + M_ex[k][1]*p[1] + M_ex[k][2]*p[2] + M_ex[k][3]*p[3])))

    P_c = [float("%.2f"%(focal*C[0]/p[2])),float("%.2f"%(focal*C[1]/p[2])),1]

    Q = []
    for k in range(3):
        Q.append(float("%.2f"%(P_in[k][0]*P_c[0] + P_in[k][1]*P_c[1] + P_in[k][2]*P_c[2])))
    for k in range(3):
        Q[k] /= Q[2]
        Q[k] = float("%.2f"%(Q[k]))

    return Q

a,b,c = newProj1Point(20,12,8)
# print(a,b,c)

def proj_1_point(x,y,z):
    denom = -sin(theta_y)*cos(theta_x)*cos(theta_z)*x + sin(theta_x)*sin(theta_z)*x + sin(theta_y)*cos(theta_x)*sin(theta_z)*y +sin(theta_x)*cos(theta_z)*y + cos(theta_x)*cos(theta_y)*z + t_z
    num_u = focal * (cos(theta_y)*cos(theta_z)*x - cos(theta_y)*sin(theta_z)*y + sin(theta_y)*z + t_x)
    num_v = focal * (sin(theta_x)*sin(theta_y)*cos(theta_z)*x + cos(theta_x)*sin(theta_z)*x - sin(theta_x)*sin(theta_y)*sin(theta_z)*y + cos(theta_z)*cos(theta_x)*y - sin(theta_x)*cos(theta_y)*z + t_x)

    u = float("%.2f"%(num_u/denom * k_x + u0))
    v = float("%.2f"%(num_v/denom * k_y + v0))

    return(u,v)

#print(proj_1_point(1,0,1))

def newProj():
    im = cv.imread("/home/persyst/Images/souris.jpg",cv.IMREAD_UNCHANGED)
    # im = cv.imread("/home/persyst/Images/road66.jpg",cv.IMREAD_UNCHANGED)
    im2 = im.copy()
    line = len(im)
    column = len(im[0])
    # print(line, column)
    print("Projection en cours...")
    for i in range(line):
        for j in range(column):
            im2[i][j]-=im2[i][j]
    for i in range(line):
        for j in range(column):
            # pt_x, pt_y, pt_z = newProj1Point(i,j,0)
            pt_x, pt_y = proj_1_point(i,j,0)
            pt_x = int(pt_x)
            pt_y = int(pt_y)
            print(pt_x,pt_y)
            if ((pt_x>=0 and pt_y>=0) and (pt_x<line and pt_y<column)):
                im2[i][j]=im[pt_x][pt_y]
    print("Projection terminee")
    cv.imshow("proj",im2)
    cv.imshow("original",im)
    cv.imwrite("proj_test.png",im2)
    cv.waitKey(0)

# newProj()

# repere cartesien
# plt.grid(True)
# plt.plot([50,100,150,200], [2,3,7,10], "b", linewidth=0.8, marker="*")
# plt.plot([50,100,150,200], [2,7,9,10], "g", linewidth=0.8, marker="+")
# plt.show()



def proj_cube():

    # t_z >= 30.0 pour la projection

    # graphique 3D
    mpl.rcParams['legend.fontsize'] = 10

    # My data
    x = [1, 2, 1, 2, 1, 2, 1, 2]
    y = [0, 0, 0, 0, 1, 1, 1, 1]
    z = [1, 1, 2, 2, 1, 1, 2, 2]

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.scatter(x[0:2], y[0:2], z[0:2], label='Cube', c='r')
    ax.scatter(x[2:4], y[2:4], z[2:4], label='Cube', c='b')
    ax.scatter(x[4:6], y[4:6], z[4:6], label='Cube', c='g')
    ax.scatter(x[6:8], y[6:8], z[6:8], label='Cube', c='black')
    ax.legend()
    ax.set_xlim(0,3)
    ax.set_ylim(-1,3)
    ax.set_zlim(0,3)
    plt.show()

    nb = len(x)
    Mp = []
    Mproj = []

    for k in range(nb):
        Mp.append([x[k],y[k],z[k]])
    for k in range(nb):
        #Mproj.append(newProj1Point(Mp[k][0],Mp[k][1],Mp[k][2]))
        Mproj.append(proj_1_point(Mp[k][0],Mp[k][1],Mp[k][2]))

    xp = []
    yp = []

    for k in range(nb):
        xp.append(Mproj[k][0])
        yp.append(Mproj[k][1])

    plt.grid(True)
    plt.scatter(xp[0:2],yp[0:2], c='r')
    plt.scatter(xp[2:4],yp[2:4], c='b')
    plt.scatter(xp[4:6],yp[4:6], c='g')
    plt.scatter(xp[6:8],yp[6:8], c='black')
    plt.show()

#proj_cube()

def proj_map():
    mpl.rcParams['legend.fontsize'] = 10

    n = 4
    x=[0,1,1,2,2,3,3,3,3,0,0,0,1,1,2,2]
    y=[0,0,3,0,3,0,1,2,3,1,2,3,1,2,1,2]
    z=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.scatter(x[0:12], y[0:12], z[0:12], c='r')
    ax.scatter(x[12:16], y[12:16], z[12:16], c='b')
    #ax.scatter(x[4:6], y[4:6], z[4:6], label='Cube', c='g')
    a#x.scatter(x[6:16], y[6:16], z[6:16], label='Cube', c='black')
    ax.legend()
    ax.set_xlim(0,4)
    ax.set_ylim(0,4)
    ax.set_zlim(0,4)
    plt.show()

    nb = len(x)
    Mp = []
    Mproj = []

    for k in range(nb):
        Mp.append([x[k],y[k],z[k]])
    for k in range(nb):
        #Mproj.append(newProj1Point(Mp[k][0],Mp[k][1],Mp[k][2]))
        Mproj.append(proj_1_point(Mp[k][0],Mp[k][1],Mp[k][2]))

    xp = []
    yp = []

    for k in range(nb):
        xp.append(Mproj[k][0])
        yp.append(Mproj[k][1])

    plt.grid(True)
    plt.scatter(xp[0:12],yp[0:12], c='r')
    plt.scatter(xp[12:16],yp[12:16], c='b')
    plt.show()

proj_map()