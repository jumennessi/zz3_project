#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import Header
from nav_msgs.msg import MapMetaData
from nav_msgs.msg import OccupancyGrid
import random

random.seed()

def map_stuff():
    map_pub = rospy.Publisher('/map', OccupancyGrid, latch=True, queue_size=1) # remove this line from create_map()
    rospy.init_node('turtlebot_map', anonymous=True)
    rate = rospy.Rate(1) # 10hz
    while not rospy.is_shutdown():
        test_map = create_map()
        map_pub.publish(test_map)
        rate.sleep()

def create_map( ):
    test_map = OccupancyGrid()
    test_map.info.resolution = 1.0 
    test_map.info.width = 10
    test_map.info.height = 10
    test_map.info.origin.position.x = random.randrange(1,100)
    test_map.info.origin.position.y = random.randrange(1,100)
    test_map.info.origin.position.z = random.randrange(1,100) 
    test_map.info.origin.orientation.x = random.randrange(1,100) 
    test_map.info.origin.orientation.y = random.randrange(1,100) 
    test_map.info.origin.orientation.z = random.randrange(1,100) 
    test_map.info.origin.orientation.w = random.randrange(1,100) 
    test_map.data = []
    # for i in range(0, 100):
    #     test_map.data.append(i)
    # print(test_map)
    return test_map 

if __name__ == '__main__':
    try:
        map_stuff()
    except rospy.ROSInterruptException:
        pass