#!/usr/bin/env python  
import roslib
# roslib.load_manifest('learning_tf')
# import rospy
# import math
# import tf
# import geometry_msgs.msg
# import turtlesim.srv

# if __name__ == '__main__':
#     rospy.init_node('turtle_tf_listener')

#     listener = tf.TransformListener()

#     rospy.wait_for_service('spawn')
#     spawner = rospy.ServiceProxy('spawn', turtlesim.srv.Spawn)
#     spawner(4, 2, 0, 'turtle2')

#     turtle_vel = rospy.Publisher('turtle2/cmd_vel', geometry_msgs.msg.Twist,queue_size=1)

#     rate = rospy.Rate(10.0)
#     while not rospy.is_shutdown():
#         try:
#             (trans,rot) = listener.lookupTransform('/turtle2', '/turtle1', rospy.Time(0))
#         except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
#             continue

#         angular = 4 * math.atan2(trans[1], trans[0])
#         linear = 0.5 * math.sqrt(trans[0] ** 2 + trans[1] ** 2)
#         cmd = geometry_msgs.msg.Twist()
#         cmd.linear.x = linear
#         cmd.angular.z = angular
#         turtle_vel.publish(cmd)

        # rate.sleep()

from tf import TransformBroadcaster
import rospy
from rospy import Time 

def main():
    rospy.init_node('my_broadcaster')
    
    b = TransformBroadcaster()
    
    translation = (0.0, 0.0, 0.0)
    rotation = (0.0, 0.0, 0.0, 1.0)
    rate = rospy.Rate(5)  # 5hz
    
    x, y = 0.0, 0.0
    
    while not rospy.is_shutdown():
        if x >= 2:
            x, y = 0.0, 0.0 
        x += 0.1
        y += 0.1
        
        translation = (x, y, 0.0)
        
        
        b.sendTransform(translation, rotation, Time.now(), '/map', '/base_link')
        rate.sleep()
    


if __name__ == '__main__':
    main()